package vezdeborg.truecode.buscounter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import vezdeborg.truecode.buscounter.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    var counter: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.countText.text = counter.toString()
        binding.minusButton.isEnabled = false

        binding.minusButton.setOnClickListener {
            if (counter > 0) {
                counter -= 1
                binding.countText.text = counter.toString()
                binding.plusButton.isEnabled = true
                binding.statusText.text = "Осталось мест: ${50 - counter}"
                binding.statusText.setTextColor(getColor(R.color.blue))
            }
            if (counter == 0) {
                binding.minusButton.isEnabled = false
                binding.statusText.text = getText(R.string.status_empty)
                binding.statusText.setTextColor(getColor(R.color.green))
            }
            if (counter == 49) {
                binding.resetButton.visibility = View.GONE
            }
        }

        binding.plusButton.setOnClickListener {
            if (counter < 50) {
                counter += 1
                binding.minusButton.isEnabled = true
                binding.countText.text = counter.toString()
                binding.statusText.text = "Осталось мест: ${50 - counter}"
                binding.statusText.setTextColor(getColor(R.color.blue))
            }
            if (counter == 50) {
                binding.plusButton.isEnabled = false
                binding.statusText.text = getText(R.string.status_full)
                binding.statusText.setTextColor(getColor(R.color.red))
                binding.resetButton.visibility = View.VISIBLE
            }
        }

        binding.resetButton.setOnClickListener {
            counter = 0
            binding.countText.text = counter.toString()
            binding.statusText.text = getText(R.string.status_empty)
            binding.resetButton.visibility = View.GONE
            binding.minusButton.isEnabled = false
            binding.plusButton.isEnabled = true
            binding.statusText.setTextColor(getColor(R.color.green))
        }
    }
}